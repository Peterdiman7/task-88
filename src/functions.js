let article = document.querySelector('.article');

anime({
    targets: article,
    translateX: 250,
    direction: 'alternate',
    loop: true,
    easing: 'spring(1, 80, 10, 0)'
  })